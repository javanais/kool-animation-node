// app/models/user.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = new mongoose.Schema ({
    local : {
    			invitation	: String,
				email       : String,
				password    : String,
				firstname   : String,
				lastname   : String,
				password_reset_key   : String,
				invitated_users : [{ type : mongoose.Schema.Types.ObjectId, ref : "User" }],
				invited_by  : { type : mongoose.Schema.Types.ObjectId, ref : "User" },
				account_activated	: Boolean,
				lang	: String,
				role 			: String,
    		}
});
// methods ======================User,

// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);