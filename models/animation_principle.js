// app/models/glossary.js
// load the things we need
var mongoose = require('mongoose');

// define the schema for our model
var schema = new mongoose.Schema ({
			lang : String,
			originalArticleID : { type : mongoose.Schema.Types.ObjectId, ref : "AnimationPrinciple" },
			title : String,
			content : String
});




// create the model for this schema and expose it to our app
module.exports = mongoose.model('AnimationPrinciple', schema);
