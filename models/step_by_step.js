// app/models/step_by_step.js
// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var schema = new mongoose.Schema ({
			lang : String,
			originalArticleID : { type : mongoose.Schema.Types.ObjectId, ref : "StepByStep" },
			sectionNumber : Number,
			title : String,
			image : String,
			question : String,
			content : String,
			templateFile : String
});

// methods ======================,


// create the model for users and expose it to our app
module.exports = mongoose.model('StepByStep', schema);
