var config = require("../config.js");
var express = require('express');
var router = express.Router();
var User   = require('../models/user');
var uuid = require('node-uuid');
var authorisedInvitationCreators=["john@lamenagerie.com","kolja@zvviks.net", "info@kool-animation.com", "julia@lamenagerie.com", "matija@zvviks.net"];

router.get('/', function(req, res) {
	User.findById(req.user._id).populate("local.invitated_users").exec(function (err, user){
		var invitedUsers=null;
		if (authorisedInvitationCreators.indexOf(req.user.local.email) > -1) {
			invitedUsers=user.local.invitated_users;
		}

		res.render('user/show', { message:'', section:'user', title:"User", user:req.user.local , invitedUsers:invitedUsers});
	});
});

router.get('/edit', function(req, res, next) {
	res.render("user/edit", { message:'', section:'user', user:req.user.local, supported_languages:config.supported_languages } );
});

router.post('/edit', function(req, res, next) {
	User.findById(req.user._id, function(err, user) {
		user.local.firstname=req.body.firstname;
		user.local.lastname=req.body.lastname;
		user.local.language=req.body.language;
		user.save(function(err) {
			if (err)
              throw err;
          	res.redirect('/user');
		});
	});
});


/*
var invitations=[ 'kool-75c73bf',
 'kool-50aacdc',
 'kool-073ab2d',
 'kool-7f0506b',
 'kool-6a86340',
 'kool-582b959',
 'kool-3453a9d',
 'kool-5109d6e',
 'kool-b7ee8e2',
 'kool-900b940',
 'kool-8902ae2',
 'kool-67c25f8',
 'kool-45f6fce',
 'kool-f4f0575',
 'kool-0d96035',
 'kool-f4209d1',
 'kool-fb53744',
 'kool-09c24cb',
 'kool-aac1ce2',
 'kool-b63ea6b',
 'kool-f0b568d',
 'kool-d41e0e4',
 'kool-15a5d1c',
 'kool-778dddf',
 'kool-1830ee8',
 'kool-12f4d16',
 'kool-7d95b08',
 'kool-c557e52',
 'kool-1e275cb',
 'kool-733ba93',
 'kool-024a0cd',
 'kool-38f9c95',
 'kool-346befc',
 'kool-a52f4e7',
 'kool-03bee38',
 'kool-b0941aa',
 'kool-a14162d',
 'kool-e29ebf3',
 'kool-7c31395',
 'kool-8d83e9f',
 'kool-7384b85',
 'kool-0f8d034',
 'kool-ba6c7e7',
 'kool-c14ff74',
 'kool-cc5904d',
 'kool-895e9d0',
 'kool-62ffc83',
 'kool-84fc1e7',
 'kool-ca466ea',
 'kool-ca56043',
 'kool-78d07fc',
 'kool-2dace71',
 'kool-50749d5',
 'kool-1c5d70f',
 'kool-f665de4',
 'kool-9e27bb9',
 'kool-e645923',
 'kool-04a770e',
 'kool-e0a4f31',
 'kool-e11b1e8',
 'kool-539f795',
 'kool-52f75be',
 'kool-b169315',
 'kool-18fefc4',
 'kool-fedc076',
 'kool-a4dcba5',
 'kool-3ba4a2e',
 'kool-1197598',
 'kool-86ed30d',
 'kool-83644e4',
 'kool-53058ce',
 'kool-3e47103',
 'kool-80948aa',
 'kool-cc957ba',
 'kool-0211f54',
 'kool-30bcf4a',
 'kool-23bc19f',
 'kool-4b0f04a',
 'kool-0ec2968',
 'kool-2839eba',
 'kool-c44ec18',
 'kool-f7bf493',
 'kool-1c0a3db',
 'kool-cd4cd0a',
 'kool-d9c40d9',
 'kool-b5f9386',
 'kool-c6a39fd',
 'kool-ca2d954',
 'kool-65d41fe',
 'kool-b552bd6',
 'kool-098b342',
 'kool-873ad53',
 'kool-f072dc1',
 'kool-9dad683',
 'kool-ff726dd',
 'kool-1847110',
 'kool-e3d6e6b',
 'kool-2e26a58',
 'kool-6d16bf4',
 'kool-1e11195',
 'kool-02305a0',
 'kool-f3533e4',
 'kool-87a705a',
 'kool-f79f389',
 'kool-51fa1ca',
 'kool-d957c76',
 'kool-8799632',
 'kool-be8db72',
 'kool-9514984',
 'kool-db13fe9',
 'kool-c91f45d',
 'kool-de045cd',
 'kool-7e996f6',
 'kool-ed94a8a',
 'kool-84151d3',
 'kool-01fd3d8',
 'kool-10898f3',
 'kool-bcd8349',
 'kool-686d471',
 'kool-b26fe42',
 'kool-70b395c',
 'kool-aa5f7b4',
 'kool-961a187',
 'kool-3c8c6f8',
 'kool-72098b3',
 'kool-566b5de',
 'kool-1d0ca0a',
 'kool-acb885c',
 'kool-e45dfdb',
 'kool-4ff9cd2',
 'kool-b203777',
 'kool-f7ccee0',
 'kool-93f9d28',
 'kool-7c4402a',
 'kool-5e0290a',
 'kool-4f7c1a3',
 'kool-c6849b9',
 'kool-c5f7484',
 'kool-dbb5ac8',
 'kool-5edea67',
 'kool-4b4beb1',
 'kool-854a340',
 'kool-dc0c904',
 'kool-e209d3c',
 'kool-08b83e2',
 'kool-a42f6e9',
 'kool-223731a',
 'kool-ee5df88',
 'kool-9f3c752',
 'kool-2b86609',
 'kool-fe0ee54',
 'kool-a86d880',
 'kool-3f2ab89',
 'kool-27da52d',
 'kool-764ddc1',
 'kool-77e0c11',
 'kool-7e7d027',
 'kool-9069290',
 'kool-2b781c3',
 'kool-28bb985',
 'kool-8ebbd45',
 'kool-0250ab5',
 'kool-d16dc9f',
 'kool-4d557c7',
 'kool-2b6e4d9',
 'kool-30fcf16',
 'kool-ce630b5',
 'kool-0c1cbb0',
 'kool-562197f',
 'kool-304ef2c',
 'kool-a7d5c1c',
 'kool-8a45ec9',
 'kool-1948523',
 'kool-ea74086',
 'kool-52c0519',
 'kool-0053a85',
 'kool-5eab5f2',
 'kool-62fe41c',
 'kool-3f206a2',
 'kool-a14ebef',
 'kool-cf23bd6',
 'kool-3859e9d',
 'kool-619518d',
 'kool-8e032e0',
 'kool-00b6566',
 'kool-8844858',
 'kool-195d951',
 'kool-4a253ac',
 'kool-adc9c43',
 'kool-03a73f6',
 'kool-a718d9e',
 'kool-45b5f44',
 'kool-9711bd6',
 'kool-747d525',
 'kool-696641a',
 'kool-8d2800e',
 'kool-be1f5e5',
 'kool-fdd2f26',
 'kool-f425eba',
 'kool-2e9436d' ];

router.get('/add-invitations', function(req, res) {

	for(var i=0; i<invitations.length; i++){
		var invitation=invitations[i];
			(function (invitation) {
						User.findById(req.user._id, function(err, user) {	
							
					      var newUser = new User();
					      newUser.local.account_activated = true;
					      newUser.local.role = "user";
					      newUser.local.lang = "en";
					      newUser.local.invited_by=req.user._id;

					      
					      newUser.local.invitation = invitation;
					      newUser.save(function(err) {
					          if (err)
					              throw err;
					          
					          user.local.invitated_users.push(newUser._id);
					          user.save(function(err){
					            if (err)
					              throw err;
					          	
					            //res.render('user', { message:'', section:'user', title:"User", user:req.user.local,  });  
					          }) 
					      });
						});    
			}(invitation));
	}  	    
		
 	res.redirect('/user');
 	//res.render('user/show', { message:'', section:'user', title:"User", user:req.user.local , invitedUsers:invitedUsers});
});
*/



/*
router.get('/createroot', function(req, res) {

	User.findOne({ 'local.invitation' :  "ROOT_INVITATION" }, function(err, invitation_account) {
		
		if(invitation_account){
			console.log("root already exists");
			res.redirect('/user');	
		} else {
			console.log("creating root account");
			var newUser = new User();
		    newUser.local.account_activated = true;
		    newUser.local.role = "user";
		    newUser.local.lang = "en";

		    newUser.local.invitation = "ROOT_INVITATION";
		    newUser.save(function(err) {
			    if (err)
			      throw err;
		    	res.redirect('/user');	  
		    });
		}
	});
});
*/

router.get('/createinvitation', function(req, res) {
  User.findById(req.user._id, function(err, user) {
      var newUser = new User();
      newUser.local.account_activated = true;
      newUser.local.role = "user";
      newUser.local.lang = "en";
      newUser.local.invited_by=req.user._id;

      var invitation = uuid.v4()
      newUser.local.invitation = invitation;
      newUser.save(function(err) {
          if (err)
              throw err;
          
          user.local.invitated_users.push(newUser._id);
          user.save(function(err){
            if (err)
              throw err;
          	res.redirect('/user');
            //res.render('user', { message:'', section:'user', title:"User", user:req.user.local,  });  
          }) 
      });
  });
	
});

module.exports = router;