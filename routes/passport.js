// app/routes.js
module.exports = function(app, passport) {
	/*
	// =====================================
	// LOGIN ===============================
	// =====================================
	// show the login form
	// process the login form in ctrl passport.js local-login
	*/
	app.post('/login', passport.authenticate('local-login', {
		successRedirect : '/home', // redirect to the secure profile section
		failureRedirect : '/login', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));
	
	// =====================================
	// SIGNUP ==============================
	// =====================================
	// show the signup form
	app.get('/signup', function(req, res) {
		// render the page and pass in any flash data if it exists
		//var email= req.body.email;
		res.render('signup.ejs', { message: req.flash('signupMessage'), email:req.session.signup_email, invitation:req.session.signup_invitation, firstname:req.session.signup_firstname, lastname:req.session.signup_lastname });
	});
	
	// process the signup form
	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/', // redirect to the secure profile section
		failureRedirect : '/signup', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));
	
	// =====================================
	// LOGOUT ==============================
	// =====================================
	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});
};
