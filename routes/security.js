var express = require('express');
var router = express.Router();
var geoip = require('geoip-lite');

router.all("/*", function(req, res, next) {
    res.set("X-Powered-By", "Javanais");
    res.render("redirect_to_kc.ejs");
    return;
    if (isLoggedIn(req)) {
        next();
        return;
    } else {
        /*
        if(req.url.indexOf("hyde") > -1){
            var geo = geoip.lookup(req.headers['x-forwarded-for'] || req.connection.remoteAddress);
            var ip=req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            
            if (geo) {
                geo.ip=ip; 
                console.log("geo", geo);
            }
            
            res.send("done");
            return;
        }
        */

        //certain page can be accessed without being logged in
        var acceptedURLArray = ["animation_principle","login", "signup", "forgot_password"];
        var isNonProtectedPage=false;
        for (var i = 0; i < acceptedURLArray.length; i++) {
            if(req.url.indexOf(acceptedURLArray[i]) > -1){
                isNonProtectedPage=true;
                break;
            }
        }

        if (isNonProtectedPage) {
            next();
            return;
        }

        res.render("login_page.ejs", {message:''});
    }
    /*
    if ( !loggedin ) {
        if(req.method=="POST" && req.url=="/login") {
        	//
            // testing to see whether a email and password has been sent
            if (!req.body || !req.body.email || !req.body.password) {
                if (!req.body ) { res.send("You're doing something fishy IP reported"); return;}
                if (!req.body.email) { res.send("Flash : Need a email to login"); return;}
                if (!req.body.password) { res.send("Flash : Need a password to login");  return;}
            } else {
            	console.log("simulating passport");
            	loggedin=true;
               f res.redirect ( "/" );
            }
        } else {
            res.render("login.ejs");
        }

    } else {
        next();
    }

    */
});

module.exports = router;

function isLoggedIn(req) {
    if (req.isAuthenticated())
        return true;
    return false;
}