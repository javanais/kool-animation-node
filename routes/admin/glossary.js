var express = require('express');
var router = express.Router();

var Glossary = require('../../models/glossary.js');
/** GET /[url] ---- show all */

router.get('/', function(req, res, next) {
  Glossary.find(function(err, glossaries) {
  	res.render("admin/glossary/list.ejs", { glossaries: glossaries });
  });
});

/** GET /[url]/new ---- show form for creating element */
router.get('/new', function(req, res, next) {
  	res.render("admin/glossary/new.ejs");
});

/** POST /[url] ----  create an element in db */
router.post('/', function(req, res, next) {
  //create element in database
  var title = req.body.title;
  var content = req.body.content;
  if (title && content) {
    Glossary.create({ title: title, content: content }, function(err, glossary) {
        res.redirect("/admin/glossary");
    });
  } else {
      res.redirect("/admin/glossary");
  }
});

/** GET /[url]/:id ----  create an element in db */
router.get('/:id', function(req, res, next) {
  Glossary.findById(req.params.id, function (err, glossary){
    res.render("admin/glossary/show.ejs", { glossary:glossary } );
  })  	
});

router.get('/:id/edit', function(req, res, next) {
  Glossary.findById(req.params.id, function (err, glossary){
    res.render("admin/glossary/edit.ejs", { glossary:glossary } );
  })    
});

var putMethod= function(req, res, next) {
  Glossary.findById(req.params.id, function (err, glossary){
    var title = req.body.title;
    var content = req.body.content;
    if (title && content) {
      glossary.title = req.body.title;
      glossary.content = req.body.content;
      glossary.save(function() {
         res.redirect("/admin/glossary"); 
      });
    }else{
      res.redirect("/admin/glossary");
    }
  });
};

var deleteMethod= function(req, res, next) {
  
  Glossary.findByIdAndRemove(req.params.id, function (err, glossary){
    res.status(401).redirect("/admin/glossary");
    req.method="get";
  })
};

router.post('/:id', function(req, res, next) {
  if(req.body._method=='PUT'){
    putMethod(req, res, next); 
  }

  if(req.body._method=='DELETE'){ 
    deleteMethod(req, res, next); 
  }
});

router.put('/:id', function (req, res, next){
  putMethod(req, res, next); 
})

router.delete('/:id', function (req, res, next){
  deleteMethod(req, res, next); 
});

 module.exports = router;