var express = require('express');
var router = express.Router();

var AnimationPrinciple = require('../../models/animation_principle.js');
/** GET /[url] ---- show all */

router.get('/', function(req, res, next) {
  AnimationPrinciple.find(function(err, animation_principles) {
  	res.render("admin/animation_principle/list.ejs", { animation_principles: animation_principles });
  });
});

/** GET /[url]/new ---- show form for creating element */
router.get('/new', function(req, res, next) {
  	res.render("admin/animation_principle/new.ejs");
});

/** POST /[url] ----  create an element in db */
router.post('/', function(req, res, next) {
  //create element in database
  var title = req.body.title;
  var content = req.body.content;
  if (title && content) {
    AnimationPrinciple.create({ title: title, content: content }, function(err, animation_principle) {
        res.redirect("/admin/animation_principle");
    });
  } else {
      res.redirect("/admin/animation_principle");
  }
});

/** GET /[url]/:id ----  create an element in db */
router.get('/:id', function(req, res, next) {
  AnimationPrinciple.findById(req.params.id, function (err, animation_principle){
    res.render("admin/animation_principle/show.ejs", { animation_principle:animation_principle } );
  })  	
});

router.get('/:id/edit', function(req, res, next) {
  AnimationPrinciple.findById(req.params.id, function (err, animation_principle){
    res.render("admin/animation_principle/edit.ejs", { animation_principle:animation_principle } );
  })    
});

var putMethod= function(req, res, next) {
  AnimationPrinciple.findById(req.params.id, function (err, animation_principle){
    var title = req.body.title;
    var content = req.body.content;
    if (title && content) {
      animation_principle.title = req.body.title;
      animation_principle.content = req.body.content;
      animation_principle.save(function() {
         res.redirect("/admin/animation_principle"); 
      });
    }else{
      res.redirect("/admin/animation_principle");
    }
  });
};

var deleteMethod= function(req, res, next) {
  
  AnimationPrinciple.findByIdAndRemove(req.params.id, function (err, animation_principle){
    res.status(401).redirect("/admin/animation_principle");
    req.method="get";
  })
};

router.post('/:id', function(req, res, next) {
  if(req.body._method=='PUT'){
    putMethod(req, res, next); 
  }

  if(req.body._method=='DELETE'){ 
    deleteMethod(req, res, next); 
  }
});

router.put('/:id', function (req, res, next){
  putMethod(req, res, next); 
})

router.delete('/:id', function (req, res, next){
  deleteMethod(req, res, next); 
});

 module.exports = router;