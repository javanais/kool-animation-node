var express = require('express');
var router = express.Router();

var StepByStep = require('../../models/step_by_step.js');
/** GET /[url] ---- show all */

router.get('/', function(req, res, next) {
  StepByStep.find(function(err, step_by_steps) {
  	res.render("admin/step_by_step/list.ejs", { step_by_steps: step_by_steps });
  });
});

/** GET /[url]/new ---- show form for creating element */
router.get('/new', function(req, res, next) {
  	res.render("admin/step_by_step/new.ejs");
});

/** POST /[url] ----  create an element in db */
router.post('/', function(req, res, next) {
  //create element in database
  var postObject={
    title: req.body.title,
    sectionNumber: req.body.sectionNumber,
    content: req.body.content,
    image: req.body.image,
    question: req.body.question,
    templateFile: req.body.templateFile,
  }
  console.log("postObject",postObject);
  if (postObject.title && postObject.sectionNumber) {
    StepByStep.create(postObject, function(err, step_by_step) {
        res.redirect("/admin/step_by_step");
    });
  } else {
      res.redirect("/admin/step_by_step");
  }
});

/** GET /[url]/:id ----  create an element in db */
router.get('/:id', function(req, res, next) {
  StepByStep.findById(req.params.id, function (err, step_by_step){
    res.render("admin/step_by_step/show.ejs", { step_by_step:step_by_step } );
  })  	
});

router.get('/:id/edit', function(req, res, next) {
  StepByStep.findById(req.params.id, function (err, step_by_step){
    res.render("admin/step_by_step/edit.ejs", { step_by_step:step_by_step } );
  })    
});

var putMethod= function(req, res, next) {
  StepByStep.findById(req.params.id, function (err, step_by_step){
    var title = req.body.title;
    var content = req.body.content;
    if (title && content) {
      step_by_step.title = req.body.title;
      step_by_step.content = req.body.content;
      step_by_step.sectionNumber = req.body.sectionNumber;
      step_by_step.image = req.body.image;
      step_by_step.question = req.body.question;
      step_by_step.templateFile = req.body.templateFile;
      step_by_step.save(function() {
         res.redirect("/admin/step_by_step"); 
      });
    }else{
      res.redirect("/admin/step_by_step");
    }
  });
};

var deleteMethod= function(req, res, next) {
  StepByStep.findByIdAndRemove(req.params.id, function (err, glossary){
    res.status(401).redirect("/admin/step_by_step");
    req.method="get";
  })
};

router.post('/:id', function(req, res, next) {
  if(req.body._method=='PUT'){
    putMethod(req, res, next); 
  }

  if(req.body._method=='DELETE'){ 
    deleteMethod(req, res, next); 
  }
});

router.put('/:id', function (req, res, next){
  putMethod(req, res, next); 
})

router.delete('/:id', function (req, res, next){
  deleteMethod(req, res, next); 
});

 module.exports = router;