var express = require('express');
var router = express.Router();
var User = require("../models/user.js");
var SendMail = require("../ctrl/sendmail.js");
var Tools = require("../lib/tools.js");
var uuid = require('node-uuid');

router.get('/', function(req, res) {
  res.render('forgot_password', { message:'', section:'login', title:"Kool Animation Forgot Password" });
});

router.post('/', function(req, res) {
  var email=req.body.email;
  console.log("trying to sending mail"+email )
  User.findOne({ 'local.email' :  email }, function(err, user) {

    if(user){
      console.log("found email")
      user.local.password_reset_key=uuid.v4();
      user.save(function(err) {
          if (err)
              throw err;
          console.log("sending mail")  
          var html = 'Hi '+user.local.firstname+ ', <b>Click to reset your password </b><br/> http://kool-animation.com/forgot_password/reset/'+user.local.password_reset_key;
          SendMail.sendmail(user.local.email, "Koolanimation Password Reset", html)
          var email_reset_message="Check you mail. A link has been sent to you by mail.";
          res.render('mail_sent', {email_reset_message:email_reset_message, message:'', section:'login', title:"Kool Animation Forgot Password" });
      });
    }else{
      res.render('forgot_password', { message:'This is not a valid account email', section:'login', title:"Kool Animation Forgot Password" });  
    }
  });
});

router.get('/reset/:passwordkey', function(req, res) {
  var passwordkey=req.params.passwordkey;
  res.render('forgot_password_reset', { message:'', section:'login', title:"Kool Animation Password Reset", password_key:req.params.passwordkey });
});

router.post('/reset', function(req, res) {
  User.findOne({ 'local.password_reset_key' :  req.body.password_key }, function(err, user) {
    if(user){
      var password_key=req.body.password_key;
      //Check that the enter email is the same as the reset email
       if(user.local.email!=req.body.email){
          res.render('forgot_password_reset', { message:'Email Incorrect', section:'login', title:"Kool Animation Password Reset", password_key:password_key });        
          return;
       }
       //check if email are not the same
       var passwordFailed=Tools.passwordFailed(req.body.password, req.body.password2)
       if(passwordFailed){
          res.render('forgot_password_reset', { message:passwordFailed, section:'login', title:"Kool Animation Password Reset", password_key:password_key });        
          return;
       }
      user.local.password=user.generateHash(req.body.password);
      user.local.password_reset_key=null;
      user.save(function(err) {
          if (err)
              throw err;
          var email_reset_message="Your password has been reset.";
          res.render('mail_sent', { email_reset_message:email_reset_message, message:'', section:'login', title:"Kool Animation Forgot Password" });                      
      });
    } else {
      res.redirect("/forgot_password");
    }
  });
  
});

module.exports = router;