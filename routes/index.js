var express = require('express');
var mongoose = require('mongoose');
var Glossary = require("../models/glossary.js");
var AnimationPrinciple = require("../models/animation_principle.js");

var StepByStep = require("../models/step_by_step.js");
var User = require("../models/user.js");
var Tools = require("../lib/tools.js");
var uuid = require('node-uuid');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  //console.log(req.isAuthenticated());
  if (req.isAuthenticated()){
    var  name = req.user.local.firstname;
    if (name === undefined || name=="" ) { name="Friend"; }
    name = Tools.capitalizeFirstLetter(name);
    res.render('home', { section:'home', title:"Kool Animation Project", name:name});  
  }else{
     res.redirect("/login");
  }
});

router.get('/login', function(req, res) {
  var msgArray =req.flash("loginMessage");
  var msg;
  if(typeof msgArray!= undefined);
    msg= msgArray.length>0 ? msgArray[0] :"";
  
  res.render('login_page', {  section:'home', message:msg,  title:"Kool Animation Login"});
});

router.get('/diy', function(req, res) {
  res.render('diy', { section:'diy', title:"Kool Animation DIY" });
});
router.get('/exercises', function(req, res) {
  res.render('exercises', { section:'exercises', title:"Kool Animation Exercises" });
});

router.get('/contact', function(req, res) {
  res.render('contact', { section:'contact', title:"Kool Animation Contact" });
});

router.get('/glossary', function(req, res) {
  Glossary.find().sort("title").exec(function (req, glossaries) {
    res.render('glossary', { section:'learn', title:"Kool Animation Glossary", glossaries: glossaries});
  });
});

router.get('/animation_principle', function(req, res) {
  AnimationPrinciple.find().sort("title").exec(function (req, animation_principles) {
    res.render('animation_principle', { section:'learn', title:"Kool Animation Principle", animation_principles: animation_principles});
  });
});

router.get('/home', function(req, res) {
    var  name = req.user.local.firstname;
    if (name === undefined || name=="") { name="Friend"; }
    
    name = Tools.capitalizeFirstLetter(name);
    res.render('home', { section:'home', title:"Kool Animation Project", name:name});  
});

router.get('/learn', function(req, res) {
    // res.render('invite', { message:'', section:'invite', title:"Kool Animation Invitation" });
		StepByStep.find().sort({ sectionNumber: 1 }).exec(function(err, stepByStep) {
      //console.log("stepByStep",stepByStep[0].content);
	   	res.render('learn', { stepByStep: stepByStep, title:"Learn", section:"learn" });
 		});
});

router.get('/download', function(req, res) {
  res.render('download', { message:'', section:'download', title:"Download" });
});

router.get('/share', function(req, res) {
  res.render('share', { message:'', section:'share', title:"Share" });
});




/*
router.get('/create_invitation', function(req, res) {
  var invitation = uuid.v4();
  var invitations=[];
  for(var i=0; i<200; i++){
    var invitation = uuid.v4()
    var fullInvitation="kool-".concat(invitation.substring(0,7));
    
    if(!(invitations.indexOf(fullInvitation) > -1)){
        invitations.push(fullInvitation);
        console.log(fullInvitation);
    }
  }
  console.log("--------------------");
  console.log(invitations)
  */
  /*

  var newUser = new User();
  newUser.local.account_activated = true;
  newUser.local.role = "newUser local";
                        // set the user's local credentials
  var invitation = uuid.v4()
  newUser.local.invitation = invitation;

  newUser.save(function(err) {
      if (err)
          throw err;
      res.render('create_invitation', { message:'', section:'invitation', invitation: invitation });      
  });
  */
//});


router.get('/glossary', function(req, res) {
	 Glossary.find().exec(function(err, glossaries) {
   		 res.render('glossary', { jsonGlossary: glossaries, title:"Glossary", section:"learn" });
	});
});

router.get('/glossary/:id', function(req, res) {
	Glossary.findById(req.params.id, function (err, glossary){
    res.send("doood", glossary);
  })
});

module.exports = router;