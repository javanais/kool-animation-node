function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function passwordFailed(password1, password2) {
    if(password1.length < 4) {
        return "Error: Password must contain at least four characters!";
    }
    /*
    re = /[0-9]/;
    if(!re.test(password1)) {
        return "Password must contain at least one number (0-9)!";        
    }
    
    re = /[a-z]/;
    if(!re.test(password1)) {
        return "Error: password must contain at least one lowercase letter (a-z)!";
    }
    
    re = /[A-Z]/;
    if(!re.test(password1)) {
        return "Error: password must contain at least one uppercase letter (A-Z)!";
    }
    */
    if(password1 != password2){
        return 'Passwords must be the same'
    }

    return false;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

module.exports.capitalizeFirstLetter=capitalizeFirstLetter;
module.exports.validateEmail = validateEmail;
module.exports.passwordFailed = passwordFailed;