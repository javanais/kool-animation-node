var express = require('express');
var session = require('express-session');
engine = require('ejs-locals');
var port = process.env.PORT || 8001;
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var flash    = require('connect-flash');
var passport = require('passport');
var routes = require('./routes/index');
var mongoose = require("mongoose");
var config = require("./config.js");
mongoose.connection.on("error", function() { 
    console.log("Erreur de connexion à la base de données")
});

mongoose.connection.on("open", function() { 
    console.log("Connexion réussie à la base de données");
});

var db = mongoose.connect (config.mongo_url);
require('./ctrl/passport')(passport); // pass passport for configuration

var app = express();
app.engine('ejs', engine);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/images/kool-ico.ico'));
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')) );
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(session({
    secret: "i_like_a_bit_of_scotch_for_islay",
    resave: true,
    saveUninitialized: true
}));

app.use(passport.initialize());
// persistent login sessions
app.use(passport.session());
app.use(flash());

app.use('/', require("./routes/security"));
require('./routes/passport.js')(app, passport);

app.use('/forgot_password', require("./routes/forgot_password"));
app.use('/', routes);

app.use('/user', require("./routes/user"));

/*
//activation admin
app.use('/admin/animation_principle', require("./routes/admin/animation_principle"));
app.use('/admin/glossary', require("./routes/admin/glossary"));
app.use('/admin/step_by_step', require("./routes/admin/step_by_step"));
*/
// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

console.log('Listening to port ',config.PORT);
app.listen(config.PORT);
module.exports = app;

/*
var Glossary = require("./models/glossary.js");
var glossary = new Glossary({ title: "Scenario", content: "Scenario content" });
glossary.save(function() {
    console.log(" sauvegardé");
});

*/


// var StepByStep = require("./models/step_by_step.js");
// var step_by_step = new StepByStep();
// step_by_step.sectionNumber = 1;
// step_by_step.title = "Storyboard";
// step_by_step.image = "";
// step_by_step.question = "question1...";
// step_by_step.content = "Quare <a href='#'> hoc </a>quidem john is trop con dear lord ill";
// step_by_step.templatePath = "";

// step_by_step.save(function() {
//     console.log(" sauvegardé step_by_step");
// });
/*
var User = require("./models/user.js");
var user = new User();
user.local.email="john@ldooodl.com";
user.save(function() {
    console.log(" sauvegardé User");
});
*/
