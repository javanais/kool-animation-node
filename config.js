module.exports = {
	'mongo_url' : 'mongodb://localhost/koolanimation',
	'baseurl' :'localhost',
	'PORT' :'8001',
	'supported_languages':["en","es","fr","sl"]
};