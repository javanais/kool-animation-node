// config/passport.js
// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;
// load up the user model
var User       		= require('../models/user');
var Tools           = require("../lib/tools");


// expose this function to our app using module.exports
module.exports = function(passport) {                                           
	// =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions                                   
    // passport needs ability to serialize and unserialize users out of session 

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
    
 	// =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
	// by default, if there was no name, it would just be called 'local'
    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },

    function(req, email, password, done) {
        //check first to see if the invitation is valid and has not been used already.
        req.session.signup_email = req.body.email;
        req.session.signup_invitation = req.body.invitation;
        req.session.signup_firstname = req.body.firstname;
        req.session.signup_lastname = req.body.lastname;


        if(!Tools.validateEmail(req.body.email)){
            return done(null, false, req.flash('signupMessage', 'We need a valid email'));
        }

        var passwordFailedMessage=Tools.passwordFailed(req.body.password, req.body.password2);
        if (passwordFailedMessage) {
            return done(null, false, req.flash('signupMessage', passwordFailedMessage));
        }

        var invitation=req.body.invitation ;//from req
        if (typeof invitation == undefined || invitation==""){
             return done(null, false, req.flash('signupMessage', 'You need an invitation to sign up.'));
        }
        //console.log("Signing up");
        /*
        User.find().sort('-local.invitation').exec(function(e, data){
           // console.log(data);
        });
        */  

       // User.findOne({ 'local.invitation' :  invitation }, function(err, invitation_account) {
             //    if (err)
             //            return done(err);

             //    if(invitation_account){
             //        //check if invitation is already attributed to this email
             //        if ( invitation_account.local.email == req.body.email ){
             //            return done(null, false, req.flash('signupMessage', 'You have already signed up. Login'));    
             //        }
             //        //console.log(invitation_account);
             //        //check if invitation is already attributed to another email
             //        //if invitation has already been used
             //        if ( invitation_account.local.email != null ){
             //            return done(null, false, req.flash('signupMessage', 'This invitation has already been used.'));    
             //        }

             //        //console.log(invitation_account.local.email);
             //        //if()
             //        //check if account has been actived
             //    } else {
             //        return done(null, false, req.flash('signupMessage', 'Invitation not valid.'));
             //    }
            	// // find a user whose email is the same as the forms email
            	// // we are checking to see if the user trying to login already exists

              

                

                User.findOne({ 'local.email' :  email }, function(err, user) {
                    // if there are any errors, return the error
                    if (err)
                        return done(err, false, req.flash('signMessage', "Sorry there was problem"));
                    // check to see if theres already a user with that email
                    if (user) {
                        return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                    } else {
                        /*
                        invitation_account.local.email=req.body.email;
                        invitation_account.local.password=invitation_account.generateHash(password);
                        invitation_account.local.firstname=req.body.firstname;
                        invitation_account.local.lastname=req.body.lastname;
                        invitation_account.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, invitation_account);
                        });
                        */
                        var newUser = new User();
                        newUser.local.account_activated = true;
                        newUser.local.role = "newUser local";
                        // set the user's local credentials
                        newUser.local.email=req.body.email;
                        newUser.local.firstname=req.body.firstname;
                        newUser.local.password = newUser.generateHash(password); // use the generateHash function in our user model
                        //newUser.local.invitation = "new _invitation";
            			// save the user
                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });
      //  });
    }));

    passport.use('local-login', new LocalStrategy ({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) { // callback with email and password from our form
        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        
        User.findOne({ 'local.email' :  email }, function(err, user) {
            
            // if there are any errors, return the error before anything else
            if (err)
                return done(err);

            // if no user is found, return the message
            if (!user)
                return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash

            // if the user is found but the password is wrong
            if (!user.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata

            // all is well, return successful user
            return done(null, user);
        });      
    }));  
};